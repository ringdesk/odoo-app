# Ringdesk integration on ODOO


## Local Setup ODOO server:
    Only works for ODOO 13.0 and above
    
    Visit https://www.odoo.com/documentation/13.0/setup/install.html for ODOO installation
    
    For More Detail Please Visit https://www.odoo.com/documentation/13.0/setup.html 


   
## Development Guide

    Copy repo inside odoo/addons
    
    Run odoo-bin.py file to start a server
    
    After sever run go to required location and login in or create db in not created

    At Setting menu update app-list and search repo name and install

    If changes in Python file restart the server and update the app. 

    XML server changes and JS changes doesn't required to restart server. Just update app and you are good to go.



## Publish in Odoo
    
    Update repo share to ODOO
    
    Visit for Detail: https://docs.huihoo.com/odoo/training/reference-material/new-odoo-apps-store-how-to-use-it.pdf

