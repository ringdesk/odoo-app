# Version 1.0

Features:

 - Click to Dial feature from ODOO phone numbers
 - Open popup with related contact on incoming/outgoing call acceptance 
 - Active call dropdown has contact/Lead tab with records matching phonenumber in addition create new contact/lead button accordingly.
 - Ringdesk Module to display Call Details with the feature to create lead if no lead attached or contact if no lead/contact attached
 - Setting Tab to enable or disable leads,Authorization type.
 - Override the peername with ODOO Full Name for active call.
 


