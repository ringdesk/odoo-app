# -*- coding: utf-8 -*-
{
    'name': 'Ringdesk Plugins For SwyxIT',
    "summary": "Integrate your swxyit with in your odoo crm",
    'version': '1.0',
    'category': 'web',
    "description": """
        This modules helps you to have call in odoo
    """,
    'author': 'Ringdesk B.V.',
    'maintainer': 'Ringdesk B.V.',
    'website': 'https://ringdesk.com',
    'depends': [
        'web','contacts','crm'
    ],
    'data': [
        'views/template.xml',
        'views/ringdesk_call_details.xml',
        'views/contact_lead_call_details_tab_view.xml',
        'views/ringdesk_settings.xml',
        'security/ir.model.access.csv',
    ],
    'qweb': [
        'static/src/xml/*.xml',
    ], 
    'js': [
        'static/src/js/ringdesk_widget.js',
        'static/src/js/ringdesk_main.js',
        'static/src/js/ringdesk_init.js',
        'https://sdk.ringdesk.com/dist/ringdesk-kit.js'
    ],
    'application': False,
    'installable': True,
    'license': 'LGPL-3',
}
